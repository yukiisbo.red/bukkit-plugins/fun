package red.yukiisbo.fun.commands;

import com.google.common.collect.ImmutableList;

public class SlapCommand extends FunInteractionCommand {

  public SlapCommand() {
    super(
        // Other
        ImmutableList.of(
            "%s slapped %s",
            "%s used their hand to slap %s",
            "%s used their hand to slap %s's face",
            "%s used their hand to slap %s's butt",
            "%s used their hand to slap %s's ass",
            "%s is angry at %s, so they slapped them",
            "%s is angry at %s, so they slapped their face",
            "%s is angry at %s, so they slapped their butt",
            "%s is angry at %s, so they slapped their ass",
            "%s is very angry at %s, so they slapped them",
            "%s is very angry at %s, so they slapped their face",
            "%s is very angry at %s, so they slapped their butt",
            "%s is very angry at %s, so they slapped their ass",
            "%s is furious and slapped %s's face",
            "%s is furious and slapped %s's butt",
            "%s is furious and slapped %s's ass",
            "%s is mad and slapped %s's face",
            "%s is mad and slapped %s's butt",
            "%s is mad and slapped %s's ass",
            "%s slapped %s's face",
            "%s slapped %s's butt",
            "%s slapped %s's ass"
        ),
        // Self
        ImmutableList.of(
            "%s slapped themselves",
            "%s slapped their own face",
            "%s slapped their own butt",
            "%s slapped their own ass",
            "%s is angry at themselves to they slapped themselves",
            "%s is angry at themselves to they slapped their own face",
            "%s is angry at themselves to they slapped their own butt",
            "%s is angry at themselves to they slapped their own ass",
            "%s is very angry at themselves to they slapped themselves",
            "%s is very angry at themselves to they slapped their own face",
            "%s is very angry at themselves to they slapped their own butt",
            "%s is very angry at themselves to they slapped their own ass",
            "%s is mad at themselves to they slapped themselves",
            "%s is mad at themselves to they slapped their own face",
            "%s is mad at themselves to they slapped their own butt",
            "%s is mad at themselves to they slapped their own ass",
            "%s is furious at themselves to they slapped themselves",
            "%s is furious at themselves to they slapped their own face",
            "%s is furious at themselves to they slapped their own butt",
            "%s is furious at themselves to they slapped their own ass"
        )
    );
  }
}
