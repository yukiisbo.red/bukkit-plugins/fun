package red.yukiisbo.fun.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import red.yukiisbo.fun.Plugin;

public abstract class BetterCommand implements CommandExecutor {
  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!doCommand(sender, command, label, args)) {
      usage(sender, command);
    }
    return true;
  }

  public abstract boolean doCommand(
      CommandSender sender, Command command,
      String label, String[] args
  );

  protected void error(CommandSender sender, String msg) {
    sender.sendMessage(Plugin.PREFIX + ChatColor.RED + msg);
  }

  protected void usage(CommandSender sender, Command command) {
    error(sender, "Usage: " + command.getUsage());
  }
}
