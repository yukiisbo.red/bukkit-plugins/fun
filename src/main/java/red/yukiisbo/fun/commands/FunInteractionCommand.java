package red.yukiisbo.fun.commands;

import com.google.common.collect.ImmutableList;
import java.util.Random;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import red.yukiisbo.fun.Plugin;

public abstract class FunInteractionCommand extends BetterCommand {

  private final ImmutableList<String> messagesOther;
  private final ImmutableList<String> messagesSelf;

  private Server server;
  private Random random;

  public FunInteractionCommand(ImmutableList<String> messagesOther,
      ImmutableList<String> messagesSelf) {
    this.messagesOther = messagesOther;
    this.messagesSelf = messagesSelf;

    server = Bukkit.getServer();
    random = new Random();
  }

  @Override
  public boolean doCommand(CommandSender sender, Command command, String label, String[] args) {
    if (args.length != 1) {
      return false;
    }

    if (!(sender instanceof Player)) {
      sender.sendMessage("What the fuck are you doing?");
      return true;
    }

    Player player = (Player) sender;

    String targetName = args[0];
    Player target = server.getPlayer(targetName);

    if (target == null) {
      error(sender, String.format("%s is not online.", targetName));
      return true;
    }

    StringBuilder sb = new StringBuilder(Plugin.PREFIX);

    String messageFormat;

    String senderName = ChatColor.BLUE + player.getName() + ChatColor.GRAY;
    targetName = ChatColor.BLUE + target.getName() + ChatColor.GRAY;

    if (player.equals(target)) {
      messageFormat = messagesSelf.get(random.nextInt(messagesSelf.size()));
      sb.append(String.format(messageFormat, senderName));
    } else {
      messageFormat = messagesOther.get(random.nextInt(messagesOther.size()));
      sb.append(String.format(messageFormat, senderName, targetName));
    }

    sb.append(".");

    server.broadcastMessage(sb.toString());
    return true;
  }
}
