package red.yukiisbo.fun.commands;

import com.google.common.collect.ImmutableList;

public class HugCommand extends FunInteractionCommand {

  public HugCommand() {
    super(
        // Other
        ImmutableList.of(
            "%s hugged %s",
            "%s gave cuddles to %s",
            "%s cuddled %s"
        ),
        // Self
        ImmutableList.of(
            "%s hug themselves",
            "%s is very lonely",
            "%s is lonely",
            "Poor %s, they're very lonely",
            "%s wants a friend"
        )
    );
  }
}
