package red.yukiisbo.fun.transformer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class MentionTransformer extends PerWordTransformer {

  private Server server;

  public MentionTransformer() {
    server = Bukkit.getServer();
  }

  @Override
  public String word(String word) {
    Player p = server.getPlayer(word);

    if (p != null) {
      p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, .5f, 1);
      return ChatColor.AQUA + p.getName() + ChatColor.RESET;
    }

    return word;
  }
}
