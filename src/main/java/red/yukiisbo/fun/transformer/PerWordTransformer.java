package red.yukiisbo.fun.transformer;

public abstract class PerWordTransformer implements Transformer {
  public abstract String word(String word);

  @Override
  public String transform(String msg) {
    StringBuilder sb = new StringBuilder();

    for (String word : msg.split(" ")) {
      sb.append(word(word)).append(" ");
    }

    String res = sb.toString();
    res = res.substring(0, res.length() - 1);
    return res;
  }
}
