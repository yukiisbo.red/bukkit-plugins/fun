package red.yukiisbo.fun.transformer;

import org.bukkit.ChatColor;

public class GreenTextTransformer implements Transformer {

  @Override
  public String transform(String msg) {
    if (msg.startsWith(">")) {
      return ChatColor.GREEN + msg;
    }
    return msg;
  }
}
