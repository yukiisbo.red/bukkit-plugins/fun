package red.yukiisbo.fun.transformer;

import org.bukkit.ChatColor;

public class CommandHighlightTransformer extends PerWordTransformer {

  @Override
  public String word(String word) {
    if (word.startsWith("/") && !word.equals("/")) {
      return ChatColor.RED + word + ChatColor.RESET;
    }
    return word;
  }
}
