package red.yukiisbo.fun.transformer;

public interface Transformer {
  String transform(String msg);
}
