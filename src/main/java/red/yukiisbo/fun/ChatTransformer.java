package red.yukiisbo.fun;

import com.google.common.collect.ImmutableList;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import red.yukiisbo.fun.transformer.Transformer;

public class ChatTransformer implements Listener {

  private final ImmutableList<Transformer> transformers;

  public ChatTransformer(ImmutableList<Transformer> transformers) {
    this.transformers = transformers;
  }

  @EventHandler
  public void onChat(AsyncPlayerChatEvent e) {
    String message = e.getMessage();

    for (Transformer t : transformers) {
      message = t.transform(message);
    }

    e.setMessage(message);
  }
}
