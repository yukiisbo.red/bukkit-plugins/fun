package red.yukiisbo.fun;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import red.yukiisbo.fun.commands.HugCommand;
import red.yukiisbo.fun.commands.SlapCommand;
import red.yukiisbo.fun.transformer.CommandHighlightTransformer;
import red.yukiisbo.fun.transformer.GreenTextTransformer;
import red.yukiisbo.fun.transformer.MentionTransformer;
import red.yukiisbo.fun.transformer.Transformer;

public class Plugin extends JavaPlugin {

  public static final String PREFIX =
      ChatColor.BLUE + "Fun" + ChatColor.GRAY + "> " + ChatColor.GRAY;

  private static final ImmutableMap<String, Class<? extends Transformer>> TRANSFORMER_MAPPING =
      ImmutableMap.of(
          "green-text", GreenTextTransformer.class,
          "command-highlight", CommandHighlightTransformer.class,
          "mention", MentionTransformer.class
      );

  private static final ImmutableMap<String, Class<? extends CommandExecutor>> COMMAND_MAPPING =
      ImmutableMap.of(
          "hug", HugCommand.class,
          "slap", SlapCommand.class
      );

  private Logger logger;

  @Override
  public void onEnable() {
    logger = getSLF4JLogger();

    saveDefaultConfig();
    startChatTransformer();
    registerCommands();
  }

  private void startChatTransformer() {
    List<String> transformerKeys = getConfig().getStringList("chat-transformers");
    List<Transformer> transformers = new ArrayList<>();

    for (String key : transformerKeys) {
      if (!TRANSFORMER_MAPPING.containsKey(key)) {
        logger.warn("{} chat transformer does not exist", key);
        continue;
      }

      try {
        Class<? extends Transformer> transformerClass = TRANSFORMER_MAPPING.get(key);
        Constructor<? extends Transformer> transformerConstructor =
            transformerClass.getConstructor();

        Transformer transformer = transformerConstructor.newInstance();
        transformers.add(transformer);

        logger.info("{} chat transformer loaded: {}",
            key, transformerClass.getName());
      } catch (NoSuchMethodException | InstantiationException |
          IllegalAccessException | InvocationTargetException e) {
        logger.error("Exception occured while loading {} chat transformer: {}",
            key, e.getMessage());
        e.printStackTrace();
      }
    }

    ImmutableList<Transformer> immutableTransformers = ImmutableList.copyOf(transformers);
    ChatTransformer chatTransformer = new ChatTransformer(immutableTransformers);

    getServer().getPluginManager().registerEvents(chatTransformer, this);

    logger.info("Chat transformer loaded and registered");
  }

  private void registerCommands() {
    List<String> commandKeys = getConfig().getStringList("commands");

    for (String key : commandKeys) {
      if (!COMMAND_MAPPING.containsKey(key)) {
        logger.warn("{} command does not exist", key);
        continue;
      }

      try {
        Class<? extends CommandExecutor> commandClass = COMMAND_MAPPING.get(key);
        Constructor<? extends CommandExecutor> commandConstructor =
            commandClass.getConstructor();

        CommandExecutor commandExecutor = commandConstructor.newInstance();

        PluginCommand command = getCommand(key);

        if (command == null) {
          logger.error(
              "Contact the developer! {} command is null because it isn't defined in plugin.yml.",
              key
          );
          continue;
        }

        command.setExecutor(commandExecutor);

        logger.info("{} command loaded and registered: {}",
            key, commandClass.getName());
      } catch (NoSuchMethodException | InstantiationException |
          IllegalAccessException | InvocationTargetException e) {
        logger.error("Exception occurred while loading {} command: {}",
            key, e.getMessage());
        e.printStackTrace();
      }
    }

    logger.info("Commands registered");
  }
}
